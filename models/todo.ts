export type ID = string;

export type Todo = {
  id: ID;
  title: string;
  description?: string;
  checked: boolean;
};

export type CreateTodo = Omit<Todo, 'id'>;
