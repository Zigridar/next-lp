import { NextRequest, NextResponse } from 'next/server';
import { MainController } from '@/globals/MainControllet';
import type { Todo, CreateTodo, ID } from '@/models/todo';

export async function POST(req: NextRequest) {
  const todo: Todo = await req.json();
  await MainController.save(todo);
  return NextResponse.json({});
}

export async function PUT(req: NextRequest) {
  const todo: CreateTodo = await req.json();
  const id = await MainController.create(todo);
  return NextResponse.json(id);
}

export async function DELETE(req: NextRequest) {
  const id: ID = await req.json();
  await MainController.delete(id.toString());
  return NextResponse.json(id);
}
