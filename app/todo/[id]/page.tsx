import { TodoForm } from '@/components/todo-form/TodoForm';
import React from 'react';
import { MainController } from '@/globals/MainControllet';

type Props = {
  params: {
    id: string;
  };
};

export default async function TodoPage({ params: { id } }: Props) {
  const todo = await MainController.getById(id);
  return <TodoForm readonly item={todo} />;
}
