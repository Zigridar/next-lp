import React from 'react';
import { TodoForm } from '@/components/todo-form/TodoForm';
import { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Создать TODO',
};

export default function Create() {
  return <TodoForm />;
}
