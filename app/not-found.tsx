import styles from './not-found.module.scss';
import Link from 'next/link';

export default function NotFound() {
  return (
    <div className={styles.notFound}>
      <p>404 Ничего не найдено :(</p>
      <Link href='/'>todos</Link>
    </div>
  );
}
