import React from 'react';
import { Todos } from '@/components/todos/Todos';
import { MainController } from '@/globals/MainControllet';

/** Страница списка */
export default async function TodosPage() {
  const todos = await MainController.get();

  return <Todos todos={todos} />;
}
