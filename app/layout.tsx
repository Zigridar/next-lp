import type { Metadata } from 'next';
import React from 'react';
import '../styles/globals.scss';
import localFont from 'next/font/local';
import { Header } from '@/components/header/Header';

const MuliFont = localFont({
  display: 'swap',
  preload: true,
  variable: '--muli-font',
  src: [
    {
      style: 'normal',
      weight: '200',
      path: '../public/fonts/Muli-ExtraLight.ttf',
    },
    {
      style: 'italic',
      weight: '200',
      path: '../public/fonts/Muli-ExtraLightItalic.ttf',
    },
    {
      style: 'normal',
      weight: '300',
      path: '../public/fonts/Muli-Light.ttf',
    },
    {
      style: 'italic',
      weight: '300',
      path: '../public/fonts/Muli-LightItalic.ttf',
    },
    {
      style: 'normal',
      weight: '400',
      path: '../public/fonts/Muli.ttf',
    },
    {
      style: 'italic',
      weight: '400',
      path: '../public/fonts/Muli-Italic.ttf',
    },
    {
      style: 'normal',
      weight: '600',
      path: '../public/fonts/Muli-SemiBold.ttf',
    },
    {
      style: 'italic',
      weight: '600',
      path: '../public/fonts/Muli-Semi-BoldItalic.ttf',
    },
    {
      style: 'normal',
      weight: '700',
      path: '../public/fonts/Muli-Bold.ttf',
    },
    {
      style: 'italic',
      weight: '700',
      path: '../public/fonts/Muli-BoldItalic.ttf',
    },
  ],
});

export const metadata: Metadata = {
  title: 'Next App',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html className={MuliFont.variable} lang='en'>
      <body suppressHydrationWarning={true}>
        <Header />
        {children}
      </body>
    </html>
  );
}
