import React from 'react';

export type GetProps<Component> = Component extends React.FC<infer Props>
  ? Props
  : never;
