import type { ID, Todo, CreateTodo } from '@/models/todo';

export interface TodoController {
  get: () => Promise<Array<Todo>>;
  getById: (id: ID) => Promise<Todo>;
  create: (todo: CreateTodo) => Promise<ID>;
  save: (todo: Todo) => Promise<void>;
  delete: (id: ID) => Promise<void>;
}
