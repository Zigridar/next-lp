import type { TodoController } from '@/controller/TodoController';
import type { CreateTodo, ID, Todo } from '@/models/todo';

const MEMORY_TODOS: Array<Todo> = [];

export class MemoryController implements TodoController {
  getById(id: ID): Promise<Todo> {
    const todo = MEMORY_TODOS.find(item => item.id === id);
    if (!todo) {
      throw new Error('404');
    }
    return Promise.resolve(todo);
  }

  create(createTodo: CreateTodo): Promise<string> {
    const id = MEMORY_TODOS.length.toString();
    MEMORY_TODOS.push({
      ...createTodo,
      id,
    });
    return Promise.resolve(id);
  }

  save(editedTodo: Todo): Promise<void> {
    const existTodoIndex = MEMORY_TODOS.findIndex(
      ({ id }) => editedTodo.id === id,
    );
    if (existTodoIndex === -1) {
      throw new Error('404');
    }
    MEMORY_TODOS[existTodoIndex] = editedTodo;
    return Promise.resolve();
  }

  get(): Promise<Array<Todo>> {
    return Promise.resolve<Array<Todo>>(MEMORY_TODOS);
  }

  delete(itemId: ID): Promise<void> {
    const existTodoIndex = MEMORY_TODOS.findIndex(({ id }) => itemId === id);
    if (existTodoIndex === -1) {
      throw new Error('404');
    }
    MEMORY_TODOS.splice(existTodoIndex, 1);
    return Promise.resolve();
  }
}
