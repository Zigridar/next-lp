import { MemoryController } from '@/controller/memoryController';
import type { TodoController } from '@/controller/TodoController';

export const MainController: TodoController = new MemoryController();
