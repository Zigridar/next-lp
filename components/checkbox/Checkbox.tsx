'use client';

import React from 'react';
import type { Todo } from '@/models/todo';
import { todoApi } from '@/fron-api';
import { useRouter } from 'next/navigation';

type CheckboxProps = {
  item: Todo;
};

export const Checkbox: React.FC<CheckboxProps> = ({ item }) => {
  const router = useRouter();

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = ({
    target: { checked },
  }) => {
    todoApi
      .edit({
        ...item,
        checked,
      })
      .then(() => {
        router.refresh();
      });
  };

  return (
    <input onChange={handleChange} checked={item.checked} type='checkbox' />
  );
};
