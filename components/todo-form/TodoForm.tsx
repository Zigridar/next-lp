'use client';
import React, { useState } from 'react';
import type { Todo } from '@/models/todo';
import { Button } from '@/components/button/Button';
import { useRouter } from 'next/navigation';
import styles from './todo-form.module.scss';
import { todoApi } from '@/fron-api';

type TodoFormProps = {
  item?: Todo;
  readonly?: boolean;
};

export const TodoForm: React.FC<TodoFormProps> = ({
  item,
  readonly = false,
}) => {
  const [edit, setEdit] = useState<boolean>(!readonly);
  const router = useRouter();

  const [todo, setTodo] = useState<Partial<Todo>>(item ?? {});

  const handleTitle: React.ChangeEventHandler<HTMLInputElement> = ({
    target: { value },
  }) => {
    setTodo(prev => ({ ...prev, title: value }));
  };

  const handleDescription: React.ChangeEventHandler<HTMLTextAreaElement> = ({
    target: { value },
  }) => {
    setTodo(prev => ({ ...prev, description: value }));
  };

  const handleCheck: React.ChangeEventHandler<HTMLInputElement> = ({
    target: { checked },
  }) => {
    setTodo(prev => ({ ...prev, checked }));
  };

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = e => {
    e.preventDefault();
    const { id, description, title, checked } = todo;
    setEdit(false);
    if (id && title) {
      todoApi
        .edit({
          id,
          description,
          title,
          checked: checked ?? false,
        })
        .then(() => router.replace(`/todo/${id}`));
    }
    if (!id && title) {
      todoApi
        .create({
          checked: checked ?? false,
          title,
          description,
        })
        .then(newId => {
          router.replace(`/todo/${newId}`);
        });
    }
  };

  const handleEdit = () => {
    setEdit(true);
  };

  const handleCancel = () => {
    router.replace('/');
  };

  const handleDelete = () => {
    if (item) {
      todoApi.delete(item.id).then(handleCancel);
    }
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      <input
        readOnly={!edit}
        value={todo.title ?? ''}
        onChange={handleTitle}
        name='title'
        type='text'
      />
      <textarea
        readOnly={!edit}
        value={todo.description}
        onChange={handleDescription}
        name='description'
      />
      <input
        checked={todo.checked ?? false}
        onChange={handleCheck}
        disabled={!edit}
        name='checked'
        type='checkbox'
      />
      {edit && (
        <div className={styles.buttons}>
          <Button type='submit'>Сохранить</Button>
          <Button onClick={handleCancel}>Отмена</Button>
        </div>
      )}
      {!edit && item && (
        <div className={styles.buttons}>
          <Button onClick={handleEdit}>Редактировать</Button>
          <Button onClick={handleDelete}>Удалить</Button>
        </div>
      )}
    </form>
  );
};
