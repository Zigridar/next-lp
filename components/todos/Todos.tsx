import React from 'react';
import { Todo } from '@/models/todo';
import { TodoItem } from '@/components/todo-item/TodoItem';
import styles from './todos.module.scss';
import { CreateButton } from '@/components/create-button/CreateButton';

type Props = {
  todos: Array<Todo>;
};

export const Todos: React.FC<Props> = ({ todos }) => (
  <main className={styles.container}>
    <div className={styles.todos}>
      {todos.map(item => (
        <TodoItem key={item.id} item={item} />
      ))}
    </div>
    <CreateButton />
  </main>
);
