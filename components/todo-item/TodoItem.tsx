import React from 'react';
import styles from './todo-item.module.scss';
import type { Todo } from '@/models/todo';
import { Checkbox } from '@/components/checkbox/Checkbox';
import { ClickWrapper } from '@/components/click-wrapper/ClickWrapper';

type TodoItemProps = {
  item: Todo;
};

export const TodoItem: React.FC<TodoItemProps> = ({ item }) => {
  return (
    <ClickWrapper id={item.id} className={styles.item}>
      <div className={styles.textWrapper}>
        <p className={styles.title}>{item.title}</p>
        {!!item.description && <p>{item.description}</p>}
      </div>
      <Checkbox item={item} />
    </ClickWrapper>
  );
};
