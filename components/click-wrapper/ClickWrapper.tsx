'use client';

import React, { PropsWithChildren } from 'react';
import { useRouter } from 'next/navigation';
import type { ID } from '@/models/todo';

type ClickWrapperProps = {
  id: ID;
  className: string;
};

export const ClickWrapper: React.FC<PropsWithChildren<ClickWrapperProps>> = ({
  children,
  id,
  className,
}) => {
  const router = useRouter();

  const handleDoubleClick = () => {
    router.replace(`/todo/${id}`);
  };

  return (
    <div className={className} onDoubleClick={handleDoubleClick}>
      {children}
    </div>
  );
};
