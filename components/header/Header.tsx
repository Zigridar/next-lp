import React from 'react';
import styles from './header.module.scss';
import { HeaderLink } from '@/components/header/HeaderLink';

type Descriptor = {
  href: string;
  title: string;
};

const descriptors: Array<Descriptor> = [
  {
    href: '/',
    title: 'todos',
  },
  {
    href: '/create',
    title: 'create',
  },
];

export const Header: React.FC = () => {
  return (
    <header className={styles.header}>
      {descriptors.map(({ href, title }) => (
        <HeaderLink className={styles.link} key={href} href={href}>
          {title}
        </HeaderLink>
      ))}
    </header>
  );
};
