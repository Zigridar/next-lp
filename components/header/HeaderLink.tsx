'use client';

import React from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import styles from './header.module.scss';
import cn from 'classnames';
import type { GetProps } from '@/types/utils';

export const HeaderLink: React.FC<GetProps<typeof Link>> = ({
  className,
  href,
  ...props
}) => {
  const pathName = usePathname();

  return (
    <Link
      {...props}
      href={href}
      className={cn(className, styles.headerLink, {
        [styles.active]: href === pathName,
      })}
    />
  );
};
