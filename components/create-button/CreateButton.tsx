'use client';

import React from 'react';
import { useRouter } from 'next/navigation';
import { Button } from '@/components/button/Button';

export const CreateButton: React.FC = () => {
  const router = useRouter();

  const handleCreate = () => {
    router.push('/create');
  };

  return <Button onClick={handleCreate}>Добавить</Button>;
};
