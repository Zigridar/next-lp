import axios, { AxiosResponse } from 'axios';
import type { CreateTodo, ID, Todo } from '@/models/todo';

const apiInstance = axios.create({
  baseURL: '/api/todos',
});

export const todoApi = {
  edit: (todo: Todo) => apiInstance.post<Todo, void>('', todo),

  create: (todo: CreateTodo) =>
    apiInstance
      .put<CreateTodo, AxiosResponse<string>>('', todo)
      .then(res => res.data),

  delete: (todoId: ID) => apiInstance.delete('', { data: todoId }),
};
